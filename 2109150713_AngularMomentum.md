<h1>Drehbewegung</h1>

<h2>Links</h2>

<a href="https://de.wikipedia.org/wiki/Tr%C3%A4gheitstensor">Wikipedia Trägheitstensor</a>

<h2>Definition Trägheitsmoment</h2>


<h3>Trägheitsmoment eines Massenpunktes</h3>

$\boxed{I = m r^2}$


<h3>Trägheitsmoment einer Menge von Massenpunkten</h3>

$\boxed{I = \sum\limits_{i=1}^{N}{m_i r_i^2} = m_i r_i^2}$

<h4>Aktuelles Trägheitsmoment in Abhängigkeit der Zeit</h4>

$\boxed{I(t) = m_i \cdot r_i^2(t)}$


<h3>Drehimpuls eines Massepunktes</h3>

$\boxed{\vec{L} = m \vec{r} \times \vec{v}}$


<h3>Drehimpuls eines Massepunktes</h3>

$\vec{L} = m \vec{r} \times \vec{v}$

mit $\vec{v} = \omega \times \vec{r}$

$\boxed{\vec{L} = m \cdot \vec{r} \times (\omega \times \vec{r})}$

Bemerkung:
Mögliche Probe bei numerischer Integration der Bewegungsgleichungen:
Für $N$ Massenpunkte in einem abgeschlossenen System muss bei einer (zeitlichen) Folge von Werten

$[t_j, \vec{r}(t_j), \vec{\omega}(t_j)], j \in [1, 1, N] \in \mathbb{N} $

der Gesamtdrehimpuls des Systems

$L = \sum\limits_{j=1}^{N}{L_j}$

konstant bleiben.


<h3>Trägheitstensor für N Massen</h3>

$\vec{L} = \sum\limits_{i}^{N}{\vec{L}_i} = \sum\limits_{i}^{N}{T_i \cdot \vec{\omega_i}}$



<!--
<h1></h1>
<h2></h2>
<h3></h3>
<h4></h4>
<h5></h5>
<h6></h6>
<h7></h7>
<b></b>
<b></b>
<b></b>
<i></i>
<i></i>
<i></i>
<></>
<></>
<></>
<></>

$A{}_{i} {}_{j} {}_{k} {}^{i} {}^{j} {}^{k}$

$A{}_{i} {}^{j} {}_{k} {}_{i} {}^{j} {}_{k}$

-->
