<h1>VT I - 01 Affine und Euklidische Vektorräume</h1>


Allgemeines
Texte werden einerseits durch pragmatische, also situationsbezogene, „textexterne“ Merkmale, andererseits durch sprachliche, „textinterne“ Merkmale bestimmt.[1] In der Sprach- und Kommunikationswissenschaft existieren viele verschiedene Textdefinitionen nebeneinander, die anhand verschiedener Textualitäts­kriterien Texte und „Nicht-Texte“ voneinander trennen. <img alt="VTI_01-2c56e3ab.png" src="media/VTI_01-2c56e3ab.png" style="float:right; height:300px;">Weiter gefasste Textbegriffe schließen auch Illustrationen oder Elemente der nonverbalen Kommunikation (etwa Mimik und Gestik) in den Text ein.Unter Umständen kann sogar eine reine Bildsequenz als Text gelten, wenn damit erkennbar eine kommunikative Funktion erfüllt wird.[3] Der Begriff des „diskontinuierlichen“ Textes aus dem Bereich der Sprachdidaktik umfasst Texte, die nicht fortlaufend geschrieben sind und sich teilweise nicht-sprachlicher Mittel bedienen, wie Formulare, Tabellen und Listen, Grafiken und Diagramme.
Texte aus Kulturen mit einer schriftlichen Überlieferungstradition unterscheiden sich in ihrem Aufbau von Texten aus Kulturen, in denen die mündliche Überlieferung eine größere Rolle spielt. In den Geisteswissenschaften werden Kulturen, von denen keine schriftlichen Dokumente überliefert sind, der Vor- und Frühgeschichte zugerechnet. Somit wird eine zwar indirekte, aber dennoch sehr bedeutsame Definition des Gegenstandes der Geschichtswissenschaft durch die Überlieferung von Texten gegeben.
Text und Schrift
Texte können mithilfe einer Schrift dargestellt werden, deren Zeichen Phoneme, Silben oder Wörter bzw. Begriffe codieren. Verschiedene Kulturen verwenden hierzu unterschiedliche Alphabete. Durch die Einführung der Schrift wurde eine Möglichkeit geschaffen, Texte, wie zum Beispiel Geschichtsschreibung, Erzählungen und Sagen, für die Nachwelt zu archivieren. Ein großer Teil des geschichtlichen Wissens stammt aus schriftlichen Aufzeichnungen, die archiviert wurden oder zufällig erhalten blieben. <img alt="VTI_01-2c56e3ab.png" src="media/VTI_01-2c56e3ab.png" style="float:right; width:50%">Texte aus Kulturen mit einer schriftlichen Überlieferungstradition unterscheiden sich in ihrem Aufbau von Texten aus Kulturen, in denen die mündliche Überlieferung eine größere Rolle spielt. In den Geisteswissenschaften werden Kulturen, von denen keine schriftlichen Dokumente überliefert sind, der Vor- und Frühgeschichte zugerechnet. Somit wird eine zwar indirekte, aber dennoch sehr bedeutsame Definition des Gegenstandes der Geschichtswissenschaft durch die Überlieferung von Texten gegeben. Texte aus Kulturen mit einer schriftlichen Überlieferungstradition unterscheiden sich in ihrem Aufbau von Texten aus Kulturen, in denen die mündliche Überlieferung eine größere Rolle spielt. In den Geisteswissenschaften werden Kulturen, von denen keine schriftlichen Dokumente überliefert sind, der Vor- und Frühgeschichte zugerechnet. Somit wird eine zwar indirekte, aber dennoch sehr bedeutsame Definition des Gegenstandes der Geschichtswissenschaft durch die Überlieferung von Texten gegeben. Texte aus Kulturen mit einer schriftlichen Überlieferungstradition unterscheiden sich in ihrem Aufbau von Texten aus Kulturen, in denen die mündliche Überlieferung eine größere Rolle spielt. In den Geisteswissenschaften werden Kulturen, von denen keine schriftlichen Dokumente überliefert sind, der Vor- und Frühgeschichte zugerechnet. Somit wird eine zwar indirekte, aber dennoch sehr bedeutsame Definition des Gegenstandes der Geschichtswissenschaft durch die Überlieferung von Texten gegeben.

Textualitätskriterien und Textdefinitionen
Wie oben erwähnt, führt eine genauere, wissenschaftliche Betrachtung zu komplexeren Definitions- und Beschreibungsversuchen. .<img alt="VTI_01-2c56e3ab.png" src="media/VTI_01-2c56e3ab.png" style="float:right; width:50%; height:300px;">Die Eigenschaft des „Text-Seins“ bezeichnet man als Textualität, die sprachwissenschaftliche Untersuchung von Texten ist die Textlinguistik. Diese Disziplin stellt verschiedene Textualitätskriterien zur Verfügung.

Robert-Alain de Beaugrande und Wolfgang Ulrich Dressler stellten 1981 eine Reihe solcher Kriterien vor. Diese Kriterien beziehen sich einerseits auf die Merkmale des Textes selbst (Kohäsion, also formaler Zusammenhalt und Kohärenz, also inhaltlicher Zusammenhalt), andererseits auf die Merkmale einer Kommunikations­situation, aus der der betreffende Text entsteht bzw. in der er eingesetzt wird (Intentionalität, Akzeptabilität, Informativität, Situationalität).

Kohäsion und Kohärenz gehören zu den am weitesten akzeptierten Textualitätskriterien, aber auch hier gibt es Abweichungen: Es gibt durchaus Texte, welche aus zusammenhanglosen Worten oder gar Lauten, zum Teil auch aus bis zu bloßen Geräuschen reduzierten Klangmalereien bestehen, und die, im Ganzen dennoch vielschichtig interpretierbar, eine eigene Art von Textualität erreichen (zum Beispiel Dada-Gedichte).

Hier kommen die situationsbezogenen Textualitätskriterien ins Spiel: Texte sind auch dadurch bestimmt, dass ein Sender sie mit einer bestimmten Absicht (Intention) produziert und/oder ein Empfänger sie als solche akzeptiert. Ob ein Text für einen bestimmten Empfänger akzeptabel ist, hängt wiederum stark davon ab, ob dieser einen Zusammenhang der empfangenen Äußerung mit seiner Situation herstellen, den Text also in seine Vorstellungswelt „einbauen“ kann (Situationalität), und ob der Text für ihn informativ ist, also in einem bestimmten Verhältnis erwartete und unerwartete, bekannte und neue Elemente enthält. Um auf das Beispiel des Dada-Gedichtes zurückzukommen: Ein nicht offensichtlich kohäsiver oder kohärenter Text kann als solcher akzeptabel sein, wenn der Empfänger davon ausgeht, dass die Intention des Senders ein hohes Maß an überraschenden oder von der Norm abweichenden Elementen im Text erfordert.

Die Intertextualität als letztes der Textualitätskriterien nach de Beaugrande und Dressler ist die Eigenschaft eines Textes, mit anderen Texten in Verbindung zu stehen und auf sie Bezug zu nehmen. In literarischen Texten geschieht dies häufig durch bewusste Verweise und Zitate, Intertextualität kann ihren Ausdruck jedoch z. B. auch darin finden, dass ein Gebrauchstext die üblichen Konventionen seiner Textsorte erfüllt.

Die einzelnen hier angeführten Textualitätskriterien sind in ihrer Interpretation durch de Beaugrande/Dressler zum Teil umstritten. Allgemein anerkannt ist, dass ein Text eine erkennbare kommunikative Funktion hat, die durch die kommunikative Absicht des Senders und die Erwartungen des Empfängers bestimmt wird, dass er als Äußerung abgegrenzt und thematisch orientiert ist, d. h. über einen inhaltlichen Kern verfügt. Eine solche Textdefinition aus kommunikativ-pragmatischer Perspektive bietet Susanne Göpferich:

„Ein Text ist ein thematisch und/oder funktional orientierter, kohärenter sprachlicher oder sprachlich-figürlicher Komplex, der mit einer bestimmten […] Kommunikationsabsicht […] geschaffen wurde, eine erkennbare kommunikative Funktion […] erfüllt und eine inhaltlich und funktional abgeschlossene Einheit bildet.“

– Göpferich, 1995, S. 56f.








<!--
<h2></h2>
<h3></h3>
<h4></h4>
<h5></h5>
<h6></h6>
<h7></h7>
-->
