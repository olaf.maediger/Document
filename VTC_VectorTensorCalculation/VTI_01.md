<h1>VT I - 01 Affine und Euklidische Vektorräume</h1>

<a href="https://www.youtube.com/watch?v=NxQTRW-5vpk&list=PLrWrjvhC1doYFByG82bJp5mGw-ZV00CUm">VT I - 01 Affine und Euklidische Vektorräume</a>

<h2>Warum Tensoren?</h2>

$\vec{p} = m \cdot \vec{v}$
$m$ : Skalar mit Wert und Einheit : $[m] = kg$
$\vec{v}$ : Vektor mit N-Werten und N-Einheiten : $[\vec{v}] = (m/s, m/s, .. , m/s)$

$\vec{L} = I \cdot \vec{\omega}$





<!--
<h2></h2>
<h3></h3>
<h4></h4>
<h5></h5>
<h6></h6>
<h7></h7>
-->
